(in-package :bot)

(define-poclo state ((game-details () game-details) 
                     (players () player-state)
                     (game-map () game-cell)) 
  camel-case)

(define-poclo game-details ((game-round "round" ()) map-width map-height round-income-energy
                            (buildings-stats () buildings-stats)) 
  camel-case)

(define-poclo buildings-stats ((defense () stats)
                              (attack () stats)
                              (energy () stats))
  screaming-snake-case)

(define-poclo stats (health construction-time price weapon-damage weapon-speed
                     weapon-cooldown-period energy-generated-per-turn destroy-multiplier
                     construction-score)
  camel-case)

(define-poclo player-state (player-type energy health hits-taken score) camel-case)

(define-poclo game-cell (x y (buildings () building-state) 
                           (missiles () missile-state) cell-owner) 
  camel-case)

(define-poclo building-state (health construction-time-left price weapon-damage
                              weapon-speed weapon-cooldown-time-left weapon-cooldown-period
                              destroy-multiplier construction-score energy-generated-per-turn
                              building-type x y player-type) 
  camel-case)

(define-poclo missile-state ((missile-speed "speed" ()) damage x y player-type) camel-case)
(define-data-class building-plan () (cost health construction-time-left
                                          construction-score damage missile-speed time-left-to-fire
                                          energy-per-turn as-integer))
(define-data-class no-op (building-plan) ())
(define-data-class map-bounds () (height width))
(define-data-class building () (health construction-time-left construction-score))
(define-data-class defence-building (building) ())
(define-data-class attack-building (building) (damage missile-speed time-left-to-fire firing-time))
(define-data-class energy-building (building) (energy-per-turn))
(define-data-class game () (map-bounds a b building-plans energy-given-per-turn))
(define-data-class buildings () (all attack defence energy unoccupied to-remove open-rows))
(define-data-class missile () (pos missile-speed damage))
(define-data-class player () (score buildings missiles energy health))
(define-data-class missiles () (current to-remove))
(define-data-class command () (x y building-plan))
(define-data-class a (player) ())
(define-data-class b (player) ())

(defun copy-hash (table &key key-copy value-copy)
  (let ((new-table (make-hash-table :size (hash-table-size table)
                                    :test (hash-table-test table)))
        (key-copy (or key-copy #'identity))
        (value-copy (or value-copy #'identity)))
    (loop for k being the hash-keys of table using (hash-value v)
       do (progn 
            (setf (gethash (funcall key-copy k) new-table) (funcall value-copy v))))
    new-table))

(defmethod copy-map-bounds ((map-bounds map-bounds))
  (make-instance 'map-bounds :height (height map-bounds)
                 :width (width map-bounds)))

(defmethod copy-building ((building attack-building))
  (make-instance 'attack-building :health (health building)
                 :construction-time-left (construction-time-left building)
                 :construction-score (construction-score building)
                 :damage (damage building)
                 :missile-speed (missile-speed building)
                 :time-left-to-fire (time-left-to-fire building)
                 :firing-time (firing-time building)))

(defmethod copy-building ((building defence-building))
  (make-instance 'defence-building :health (health building)
                 :construction-time-left (construction-time-left building)
                 :construction-score (construction-score building)))

(defmethod copy-building ((building energy-building))
  (make-instance 'energy-building :health (health building)
                 :construction-time-left (construction-time-left building)
                 :construction-score (construction-score building)
                 :energy-per-turn (energy-per-turn building)))

(defun copy-unoccupied (unoccupied)
  (make-array (length unoccupied) :adjustable t
                                    :fill-pointer (fill-pointer unoccupied)
                                    ::initial-contents unoccupied))

(defmethod copy-buildings ((buildings buildings))
  (make-instance 'buildings :all (copy-hash (all buildings) :value-copy #'copy-building)
                 :attack (copy-hash (attack buildings) :key-copy #'copy-building)
                 :defence (copy-hash (defence buildings) :key-copy #'copy-building) 
                 :energy (copy-hash (energy buildings) :key-copy #'copy-building)
                 :unoccupied (copy-unoccupied (unoccupied buildings))
                 :to-remove (copy-hash (to-remove buildings))
                 :open-rows (copy-hash (open-rows buildings))))

(defmethod copy-missile ((missile missile))
  (make-instance 'missile :pos (pos missile)
                 :missile-speed (missile-speed missile)
                 :damage (damage missile)))

(defmethod copy-missiles ((missiles missiles))
  (make-instance 'missiles :current (copy-hash (current missiles) :key-copy #'copy-missile)
                 :to-remove (copy-hash (to-remove missiles) :key-copy #'copy-missile)))

(defmethod copy-player ((a a))
  (make-instance 'a :score (score a) 
                 :buildings (copy-buildings (buildings a))
                 :missiles (copy-missiles (missiles a))
                 :energy (energy a)
                 :health (health a)))

(defmethod copy-player ((b b))
  (make-instance 'b :score (score b) 
                 :buildings (copy-buildings (buildings b))
                 :missiles (copy-missiles (missiles b))
                 :energy (energy b)
                 :health (health b)))

(defmethod copy-game ((game game))
  (make-instance 'game :map-bounds (copy-map-bounds (map-bounds game))
                 :a (copy-player (a game))
                 :b (copy-player (b game))
                 :building-plans (building-plans game)
                 :energy-given-per-turn (energy-given-per-turn game)))

(defun create-no-op ()
  (make-instance 'no-op :cost 0 :health 0 :construction-time-left 0
                 :construction-score 0 :damage 0 :missile-speed 0
                 :time-left-to-fire 0 :energy-per-turn 0 :as-integer -1))

(defmethod create-attack-building ((building-plan building-plan))
  (make-instance 'attack-building :health (health building-plan)
                 :construction-time-left (construction-time-left building-plan)
                 :construction-score (construction-score building-plan)
                 :damage (damage building-plan)
                 :missile-speed (missile-speed building-plan)
                 :time-left-to-fire (time-left-to-fire building-plan)
                 :firing-time (time-left-to-fire building-plan)))

(defmethod create-energy-building ((building-plan building-plan))
  (make-instance 'energy-building :health (health building-plan)
                 :construction-time-left (construction-time-left building-plan)
                 :construction-score (construction-score building-plan)
                 :energy-per-turn (energy-per-turn building-plan)))

(defmethod create-defence-building ((building-plan building-plan))
  (make-instance 'defence-building :health (health building-plan)
                 :construction-time-left (construction-time-left building-plan)
                 :construction-score (construction-score building-plan)))

(defmethod get-missile-speed ((missile-state missile-state))
  (if (belongs-to-a missile-state)
      (missile-speed missile-state)
      (- (missile-speed missile-state))))

(defmethod create-missile ((cell game-cell) (missile-state missile-state))
  (make-instance 'missile :pos (cons (y cell) (x cell)) 
                 :missile-speed (get-missile-speed missile-state)
                 :damage (damage missile-state)))

(defmethod energy-building-from-state ((state building-state))
  (make-instance 'energy-building :health (health state)
                 :construction-time-left (construction-time-left state)
                 :construction-score (construction-score state)
                 :energy-per-turn (energy-generated-per-turn state)))

(defmethod attack-building-from-state ((state building-state))
  (make-instance 'attack-building :health (health state)
                 :construction-time-left (construction-time-left state)
                 :construction-score (construction-score state)
                 :damage (weapon-damage state)
                 :missile-speed (weapon-speed state)
                 :time-left-to-fire (weapon-cooldown-time-left state)
                 :firing-time (weapon-cooldown-time-left state)))

(defmethod defence-building-from-state ((state building-state))
  (make-instance 'defence-building :health (health state)
                 :construction-time-left (construction-time-left state)
                 :construction-score (construction-score state)))

(defmethod building-from-state ((state building-state))
  (cond ((> (energy-generated-per-turn state) 0)
         (energy-building-from-state state))
        ((> (weapon-damage state) 0)
         (attack-building-from-state state))
        (t (defence-building-from-state state))))

(defun belongs-to-a (state)
  (string= (player-type state) "A"))

(defmethod player-from-state ((player-state player-state) (game game))
  (with-slots (height width) (map-bounds game)
    (if (belongs-to-a player-state)
        (make-instance 'a :score (score player-state)
                 :buildings (make-instance 'buildings
                                           :all (make-hash-table :test 'equal)
                                           :attack (make-hash-table)
                                           :defence (make-hash-table)
                                           :energy (make-hash-table)
                                           :unoccupied (make-array
                                                        (+ (floor (* height width) 2) 1)
                                                        :adjustable t
                                                        :fill-pointer 0)
                                           :to-remove (make-hash-table)
                                           :open-rows (make-hash-table))
                 :missiles (make-instance 'missiles :current (make-hash-table) 
                                          :to-remove (make-hash-table))
                 :energy (energy player-state)
                 :health (health player-state))
        (make-instance 'b :score (score player-state)
                 :buildings (make-instance 'buildings
                                           :all (make-hash-table :test 'equal)
                                           :attack (make-hash-table)
                                           :defence (make-hash-table)
                                           :energy (make-hash-table)
                                           :unoccupied (make-array
                                                        (+ (floor (* height width) 2) 1)
                                                        :adjustable t
                                                        :fill-pointer 0)
                                           :to-remove (make-hash-table)
                                           :open-rows (make-hash-table))
                 :missiles (make-instance 'missiles :current (make-hash-table) 
                                          :to-remove (make-hash-table))
                 :energy (energy player-state)
                 :health (health player-state)))))

(defmethod map-bounds-from-state ((state state))
  (with-slots (map-height map-width) (game-details state)
    (make-instance 'map-bounds :height map-height :width map-width)))

(defmethod create-players ((state state) (game game))
  (loop for player-state across (players state)
     do (let ((new-player (player-from-state player-state game)))
          (if (belongs-to-a player-state)
              (setf (a game) new-player)
              (setf (b game) new-player)))))

(defmethod create-missiles ((state state) (game game))
  (with-slots (game-map) state
    (loop for row across game-map
         do (loop for cell across row
               when (> (length (missiles cell)) 0)
               do (loop for missile-state across (missiles cell)
                     do (let ((new-missile (create-missile cell missile-state)))
                          (if (belongs-to-a missile-state)
                              (setf (gethash new-missile (current (missiles (a game)))) t)
                              (setf (gethash new-missile (current (missiles (b game)))) t))))))))

(defmethod add-building ((player player) (building building) pos)
  (setf (gethash pos (all (buildings player))) building))

(defmethod add-building ((player player) (building attack-building) pos)
  (progn
    (setf (gethash building (attack (buildings player))) pos)
    (call-next-method player building pos)))

(defmethod add-building ((player player) (building energy-building) pos)
  (progn 
    (setf (gethash building (energy (buildings player))) pos)
    (call-next-method player building pos)))

(defmethod buildings-from-state ((state state) (game game))
  (with-slots (game-map) state
    (loop for row across game-map
         do (loop for cell across row
               when (> (length (buildings cell)) 0)
               do (loop for building-state across (buildings cell)
                     do (let ((new-building (building-from-state building-state)))
                          (if (belongs-to-a building-state)
                              (add-building (a game) new-building (cons (y cell) (x cell)))
                              (add-building (b game) new-building (cons (y cell) (x cell))))))))))

(defmethod building-plan-from-state ((stats stats))
  (make-instance 'building-plan :cost (price stats)
                 :health (health stats)
                 :construction-time-left (construction-time stats)
                 :construction-score (construction-score stats)
                 :damage (weapon-damage stats)
                 :missile-speed (weapon-speed stats)
                 :time-left-to-fire (weapon-cooldown-period stats)
                 :energy-per-turn (energy-generated-per-turn stats)))

(defmethod building-plans-from-state ((state state) (game game))
  (with-accessors ((plans building-plans)) game
    (with-slots (defense attack energy) (buildings-stats (game-details state))
      (setf plans (make-array 4 :initial-contents (list 
                              (building-plan-from-state defense)
                              (building-plan-from-state attack)
                              (building-plan-from-state energy)
                              (create-no-op)))))))

(defmethod in-left-half ((cell game-cell) (map-bounds map-bounds))
  (< (x cell) (floor (width map-bounds) 2)))

(defmethod initialize-open-rows ((state state) (player player))
  (with-slots (unoccupied open-rows) (buildings player)
    (loop for pos across unoccupied
       when (not (gethash (cdr pos) open-rows))
       do (setf (gethash (cdr pos) open-rows) t))))

(defmethod initialize-unoccupied ((state state) (game game))
  (with-slots (a b map-bounds) game
    (loop for row across (game-map state)
       do (loop for cell across row
             do (if (in-left-half cell map-bounds)
                    (if (= (length (buildings cell)) 0)
                        (vector-push-extend (cons (y cell) (x cell))
                                            (unoccupied (buildings a))))
                    (if (= (length (buildings cell)) 0)
                        (vector-push-extend (cons (y cell) (x cell)) 
                                            (unoccupied (buildings b)))))))))

(defmethod game-from-state ((state state))
  (let ((game (make-instance 'game :map-bounds (map-bounds-from-state state)
                 :a nil
                 :b nil
                 :building-plans nil
                 :energy-given-per-turn (round-income-energy (game-details state)))))
    (create-players state game)
    (buildings-from-state state game)
    (create-missiles state game)
    (building-plans-from-state state game)
    (initialize-unoccupied state game)
    (initialize-open-rows state (a game))
    (initialize-open-rows state (b game))
    game))

(defun read-game (filename)
  (with-open-file (file filename)
    (when file
      (game-from-state (parse-state file)))))
