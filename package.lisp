(defpackage :bot
  (:use :cl :yason)
  (:export #:make-move
           #:log-to-file
           #:define-poclo
           #:camel-case
           #:snake-case
           #:screaming-snake-case))
