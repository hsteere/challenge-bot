(in-package :bot)

(defparameter *state-path* "state.json")
(defparameter *command-path* "command.txt")
(defparameter *random-state-path* "random-state")

(defmethod as-integer ((building-plan building-plan))
  (with-slots (damage energy-per-turn) building-plan
    (cond ((> damage 0) 1)
          ((> energy-per-turn 0) 2)
          (t 0))))

(defmethod write-command-to-stream ((command command) out)
  (with-slots (x y building-plan) command
      (format out "~a,~a,~a~%" x y (as-integer building-plan))))

(defmethod write-command (filename (plan no-op) value pos) (write-empty))

(defmethod write-command (filename (plan building-plan) value pos)
  (with-open-file (file filename :direction :output :if-does-not-exist :create 
                        :if-exists :supersede)
    (when file
      (let ((command (make-instance 'command 
                                    :x (cdr pos) 
                                    :y (car pos)
                                    :building-plan plan)))
        (write-command-to-stream command file)))))

(defun generate-random-state ()
  (let ((*print-readably* t))
    (setf *random-state* (make-random-state))
    (with-open-file (file *random-state-path*
                          :direction :output :if-exists :supersede :if-does-not-exist :create)
      (when file
        (write *random-state* :stream file)))))

(defun set-random-state-from-file ()
  (with-open-file (file *random-state-path*)
    (if file
        (setf *random-state* (read file))
        (generate-random-state))))

(defun set-random-state ()
  (if (probe-file *random-state-path*)
      (set-random-state-from-file)
      (generate-random-state)))

(defun write-empty ()
  (with-open-file (file *command-path* :direction :output
                        :if-does-not-exist :create 
                        :if-exists :supersede)
    (when file (format file " "))))

(defun make-move ()
  (let ((game (read-game *state-path*)))
    (setf lparallel:*kernel* (lparallel:make-kernel 4))
    (set-random-state)
    (let ((result (mcts game 50 20 50)))
      (if result
          (destructuring-bind (value building-plan pos) result
            (declare (ignore value))
            (if pos
                (write-command *command-path* building-plan value pos)))
          (write-empty)))))
