(in-package :cl-user)

(defpackage :bot.sys
  (:use :asdf :cl))

(in-package :bot.sys)

(defsystem :bot
  :name "Bot"
  :author "Henry Steere"
  :version "0.0.1"
  :maintainer "henry.steere@gmail.com"
  :license "BSD"
  :description "Entellect challenge minimax bot"
  :long-description "Bot for the Entellect Challenge using a minimax strategy."
  :depends-on (:yason :alexandria :cl-ppcre :lparallel :bordeaux-threads 
                      :queues :queues.simple-cqueue)
  :components ((:file "package")
               (:file "parsing" :depends-on ("package"))
               (:file "mcts" :depends-on ("package" "parsing" "state"))
               (:file "state" :depends-on ("package" "parsing"))
               (:file "bot" :depends-on ("package" "parsing" "state" "mcts"))))
