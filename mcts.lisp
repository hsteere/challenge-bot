(in-package :bot)

(defmethod create-building ((building-plan building-plan) (player player))
  (progn 
    (decf (energy player) (cost building-plan))
    (cond ((> (damage building-plan) 0)
           (create-attack-building building-plan))
          ((> (energy-per-turn building-plan) 0)
           (create-energy-building building-plan))
          (t (create-defence-building building-plan)))))

(defmethod complete ((building building))
  (< (construction-time-left building) 0))

(defmethod collides (pos speed building-pos (building building))
  (and (complete building)
       (= (car pos) (car building-pos))
       (or (= (cdr pos) (cdr building-pos))
           (= (signum (- (+ (cdr pos) speed) (cdr building-pos)))
              (signum (- (cdr building-pos) (cdr pos)))))))

(defmethod update-construction-time ((player player))
  (loop for building being the hash-values of (all (buildings player))
     when (>= (construction-time-left building) 0)
     do (let ((current-building building))
          (decf (construction-time-left current-building))
          (if (< (construction-time-left current-building) 0)
              (incf (score player) (construction-score building))))))

(defmethod detect-collision ((buildings buildings) (missile missile))
  (with-slots (pos missile-speed) missile
    (loop for building-pos being the hash-keys of (all buildings)
       using (hash-value building)
       until collision
       when (collides pos missile-speed building-pos building)   
       collect building-pos into collision
       finally (return (if (not (null collision))
                           (car collision)
                           nil)))))

(defmethod inflict-damage (pos (missile missile) (attacker player) (defender player))
  (with-slots (buildings missiles) attacker
    (let ((building (gethash pos (all (buildings defender)))))
      (decf (health building) (damage missile))
      (setf (gethash missile (to-remove missiles)) t)
      (incf (score attacker) (damage missile)))))

(defmethod remove-buildings ((buildings buildings))
  (with-slots (all attack defence energy unoccupied to-remove) buildings
    (loop for building being the hash-values of all
       using (hash-key pos)
       when (<= (health building) 0)
       do (let ((current-building building))
            (cond ((gethash building attack)
                   (remhash building attack))
                  ((gethash building defence)
                   (remhash building defence))
                  ((gethash building energy)
                   (remhash building energy)))
            (vector-push-extend pos unoccupied)
            (setf (gethash pos to-remove) t)))
    (remove-marked-buildings buildings)))

(defmethod remove-marked-buildings ((buildings buildings))
  (with-slots (all to-remove) buildings
    (loop for pos being the hash-keys of to-remove
       do (remhash pos all))
    (clrhash to-remove)))

(defmethod advance-position ((missile missile))
  (with-slots (pos missile-speed) missile
    (setf (pos missile) (cons (car pos) (+ (missile-speed missile) (cdr pos))))
    pos))

(defmethod reverse-position ((missile missile))
  (with-slots (pos missile-speed) missile
    (setf (pos missile) (cons (car pos) (- (cdr pos) (missile-speed missile))))
    (if (>= (cdr (pos missile)) 8)
        (error "POS ~a OUT OF BOUNDS AFTER REVERSE~%" (pos missile)))
    pos))

(defmethod move ((game game) (missile missile))
  (with-slots (map-bounds a b) game
    (declare (optimize (debug 3) (speed 0)))
    (if (or (>= (cdr (pos missile)) (width map-bounds))
            (< (cdr (pos missile)) 0))
        (error (format nil "POS ~a is out of bounds" (pos missile))))
    (let ((pos (advance-position missile)))
      (if (or (>= (cdr pos) (width map-bounds))
              (< (cdr pos) 0))
          (let* ((defender (if (>= (cdr pos) (width map-bounds)) b a))
                 (attacker (if (>= (cdr pos) (width map-bounds)) a b))
                 (missiles (missiles attacker)))
            (setf (gethash missile (to-remove missiles)) t)
            (decf (health defender) (damage missile))
            (incf (score attacker) (damage missile)))))))

(defmethod update-missile-position ((missile missile) (game game)
                                    (attacker player) (defender player)
                                    (map-bounds map-bounds))
  (let ((collision-location (detect-collision (buildings defender) missile)))
      (if collision-location
          (inflict-damage collision-location missile attacker defender)
          (move game missile))))

(defmethod move-missiles ((map-bounds map-bounds) (game game)
                          (attacker player) (defender player))
  (loop for missile being the hash-keys of (current (missiles attacker))
     do (update-missile-position missile game attacker defender map-bounds))
  (remove-destroyed-missiles (missiles attacker)))

(defmethod remove-destroyed-missiles ((missiles missiles))
  (loop for missile being the hash-keys of (to-remove missiles)
     do (remhash missile (current missiles)))
  (clrhash (to-remove missiles)))

(defmethod make-missile ((building attack-building) pos (player a))
  (with-slots (damage missile-speed) building
      (make-instance 'missile :damage damage :pos (cons (car pos) (cdr pos))
                     :missile-speed missile-speed)))

(defmethod make-missile ((building attack-building) pos (player b))
  (with-slots (damage missile-speed) building
      (make-instance 'missile :damage damage :pos (cons (car pos) (cdr pos))
                     :missile-speed (- missile-speed))))

(defmethod fire-missile (pos (building attack-building)
                         (missiles missiles) (player player))
  (with-slots (current) missiles
    (let ((time-left (time-left-to-fire building)))
      (if (= time-left 0)
          (let ((missile (make-missile building pos player)))
            (setf (gethash missile current) t)
            (setf (time-left-to-fire building) 
                  (firing-time building)))
          (decf (time-left-to-fire building))))))

(defmethod fire-missiles ((player player))
  (with-slots (buildings missiles) player
    (loop for pos being the hash-values of (attack buildings) 
       using (hash-key building)
       do (fire-missile pos building missiles player))))

(defparameter *min* (- (expt 2 64)))
(defparameter *max* (- (expt 2 64) 1))

(defun shuffle (arr)
  (let ((n (length arr)))
    (loop for i from 1 to (- n 1)
       do (let ((j (+ i (random (- n i)))))
            (rotatef (aref arr i) (aref arr j))))))

(defun log-to-file (message arg)
  (with-open-file (f "log" :direction :output :if-exists :append :if-does-not-exist :create)
    (when f
      (if arg
          (format f message arg)
          (format f message)))))

(defmethod can-build ((building-plan building-plan) (player player))
  (and (> (length (unoccupied (buildings player))) 0)
       (<= (cost building-plan) (energy player))))

(defmethod add-building-to-board ((building building) pos (player player))
  (with-slots (all unoccupied) (buildings player)
    (setf (gethash pos all) building)))

(defmethod add-building-to-board ((building attack-building) pos (player player))
  (with-slots (attack) (buildings player)
    (setf (gethash building attack) pos)
    (call-next-method building pos player)))

(defmethod add-building-to-board ((building energy-building) pos (player player))
  (with-slots (energy) (buildings player)
    (setf (gethash building energy) pos)
    (call-next-method building pos player)))

(defmethod add-building-to-board ((building defence-building) pos (player player))
  (with-slots (defence) (buildings player)
    (setf (gethash building defence) pos)
    (call-next-method building pos player)))

(defmethod build ((no-op no-op) (player player)) ())

(defmethod build ((building-plan building-plan) (player player))
  (let ((new-building (create-building building-plan player))
        (pos (vector-pop (unoccupied (buildings player)))))
    (add-building-to-board new-building pos player)
    pos))

(defparameter *no-op* (create-no-op))

(defmethod select-building-plan ((player player) (game game))
  (if (= (length (unoccupied (buildings player))) 0)
      *no-op*
      (with-slots (building-plans) game
        (let ((available (remove-if-not (lambda (plan) (can-build plan player)) building-plans)))
          (if (> (length available) 0)
              (aref available (random (length available)))
              *no-op*)))))

(defun make-minimax-key (max-player max-turn depth)
  (+ (player-enum max-player)
     (ash (max-turn-enum max-turn) 1)
     (ash depth 3)))

(defmethod other-player ((player a) (game game)) (b game))
(defmethod other-player ((player b) (game game)) (a game))

(defparameter *best-score* (- (expt 2 63) 1))
(defparameter *worst-score* (- (expt 2 63)))

(defmethod dead ((player player)) (<= (health player) 0))

(defmethod find-vulnerable-row ((player player) (game game))
  (with-slots (open-rows) (buildings player)
    (loop for pos being the hash-values of (attack (buildings player))
       when (gethash (cdr pos) open-rows)
       do (return (cdr pos)))))

(defmethod select-building-position ((player player) (game game) (plan building-plan))
  (with-slots (unoccupied) (buildings player)
    (let ((vulnerable-row (find-vulnerable-row player game)))
      (if vulnerable-row
          (let ((first-vulnerable (find-if (lambda (pos) (= (cdr pos) vulnerable-row))
                                           unoccupied)))
            (delete first-vulnerable unoccupied :test 'equal)
            first-vulnerable)
          (vector-pop unoccupied)))))

(defmethod select-building-position ((player player) (game game) (no-op no-op)) '(0 . 0))

(defmethod attacking-row-score ((player player) (game game))
  (let ((building-score 0)
        (missile-score 0))
    (with-slots (open-rows) (buildings (other-player player game))
      (with-slots (width height) (map-bounds game)
        (loop for pos being the hash-values of (attack (buildings player))
           when (gethash (cdr pos) open-rows)
           do (incf building-score 500))
        (loop for missile being the hash-keys of (current (missiles player))
           when (gethash (cdr (pos missile)) open-rows)
           do (incf missile-score 200))))
    (+ building-score missile-score)))

(defmethod player-state-value ((player player) (game game))
  (+ (attacking-row-score player game)
     (score player)))

(defmethod state-value ((a a) (b b) (game game))
  (cond ((dead a) 10000)
        ((dead b) -10000)
        (t (- (player-state-value a game) 
              (player-state-value b game)))))

(defmethod mcts ((game game) depth number-of-candidates spikes)
  (let ((move-candidates (select-move-candidates game number-of-candidates)))
    (if (null move-candidates)
        nil
        (let ((tasks (mapcar (lambda (candidate)
                               (lparallel:future 
                                 (destructuring-bind (pos plan) candidate
                                   (list (move-value (copy-game game)
                                                     pos
                                                     plan
                                                     depth
                                                     spikes)
                                         plan pos))))
                             move-candidates)))
          (sleep 1.7)
          (reduce (lambda (a b) 
                    (if (not (lparallel:fulfilledp b))
                        a
                        (let ((value (lparallel:force b)))
                          (if (> (car a) (car value))
                              a value)))) 
                  tasks :initial-value (list 0 *no-op* '(0 . 0)))))))

(defmethod select-move-candidates ((game game) number-of-candidates)
  (let* ((actual-candidates (min (length (unoccupied (buildings (a game)))) number-of-candidates))
         (game-copy (copy-game game))
         (plan (select-building-plan (a game-copy) game-copy))
         (building-position (select-building-position (a game) game-copy plan)))
    (if (= actual-candidates 0)
        nil
        (with-slots (unoccupied) (buildings (a game-copy))
          (shuffle unoccupied)
          (loop for i from 0 to (- actual-candidates 1)
             collect (list building-position plan))))))

(defmethod move-value ((game game) pos (plan building-plan) depth spikes)
  (with-slots (a) game
    (let ((total 0))
      (add-building-to-board (create-building plan a) pos a)
      (loop for i from 1 to spikes 
         do (let ((simulation-result (mcts-simulation (copy-game game) depth)))
              (incf total simulation-result)))
      (/ (float total) spikes))))

(defmethod mcts-simulation ((game game) depth)
  (with-slots (a b) game
    (let ((building-plan (select-building-plan a game)))
      (labels ((recur (depth)
                 (if (or (= depth 0) 
                         (dead a)
                         (dead b))
                     (state-value a b game)
                     (progn
                       (player-move a)
                       (player-move b)
                       (game-turn game)
                       (recur (- depth 1)))))
               (player-move (player)
                 (build (select-building-plan player game) player)))
        (let ((pos (build building-plan a))) 
          (game-turn game)
          (recur depth))))))

(defun update-energy (player game)
  (loop for building being the hash-keys of (energy (buildings player))
     do (incf (energy player) (energy-per-turn building)))
  (incf (energy player) (energy-given-per-turn game)))

(defmethod game-turn ((game game))
  (with-slots (map-bounds a b) game
    (progn (update-construction-time a)
           (update-construction-time b)
           (fire-missiles a)
           (fire-missiles b)
           (move-missiles map-bounds game a b)
           (move-missiles map-bounds game b a)
           (update-energy a game)
           (update-energy b game))))
